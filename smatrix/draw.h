#ifndef _draw_h
#define _draw_h

#include "matrix.h"
#include "draw.c"

void
draw(slist **first, struct matrix *matrix, struct winsize *terminal_dimensions);

#endif
